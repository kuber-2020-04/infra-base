terraform {
  backend gcs {
    bucket = "kubernetes-production-tfstate"
    prefix = "terraform/state"
  }
}