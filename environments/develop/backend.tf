terraform {
  backend gcs {
    bucket = "kubernetes-develop-tfstate"
    prefix = "terraform/state"
  }
}