## Providers

No provider.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| name | Environment name | `string` | `"develop"` | no |
| project | GCP project ID | `string` | n/a | yes |
| region | GCP region | `string` | `"europe-north1"` | no |

## Outputs

| Name | Description |
|------|-------------|
| develop\_output | n/a |

