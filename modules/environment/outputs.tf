output cluster_endpoint {
  description = "Public endpoint of Kubernetes cluster"
  value       = "${google_container_cluster.cluster.endpoint}"
}
output service_external_ip_address {
  description = "External IP address for ingress"
  value       = "${google_compute_address.address.address}"
}
